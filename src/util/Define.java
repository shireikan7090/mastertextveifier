package util;

public class Define
{
    //TODO: 環境設定ファイルで定義するようにいずれ
    public static final String MASTER_BASE_DIRECTORY = "/Users/h-999-sugita/Documents/game_master_jp/spt/";

    public static final String RARITY_SR = "SR";
    public static final String RARITY_SSR = "SSR";
    public static final String RARITY_UR = "UR";
    public static final String[] RARITIES = {
            RARITY_SR,
            RARITY_SSR,
            RARITY_UR
    };

    public static final int LANGUAGE_REGION_JP = 0;
    public static final int LANGUAGE_REGION_WW = 1;

    public static final String LANGUAGE_DEFAULT = "";
    public static final String LANGUAGE_DE = "de";
    public static final String LANGUAGE_ES = "es";
    public static final String LANGUAGE_FR = "fr";
    public static final String LANGUAGE_IT = "it";
    public static final String LANGUAGE_PT = "pt";
    public static final String LANGUAGE_ZH = "zh";
    public static final String LANGUAGE_KR = "kr";
    public static final String[][] LANGUAGES = {
            {
                LANGUAGE_DEFAULT,
            },
            {
                LANGUAGE_DEFAULT,
                LANGUAGE_DE,
                LANGUAGE_ES,
                LANGUAGE_FR,
                LANGUAGE_IT,
                LANGUAGE_PT,
                LANGUAGE_ZH,
                LANGUAGE_KR,
            }
    };

    public static String[] getLocalizedColumnName(int region, String columnName)
    {
        String[] columnNames = new String[LANGUAGES[region].length];
        for(int i=0; i<LANGUAGES[region].length; i++)
        {
            String fixedColumnName = columnName;
            if(0 < i)
            {
                fixedColumnName = String.format("%s_%s",columnName, LANGUAGES[region][i]);
            }
            columnNames[i] = fixedColumnName;
        }
        return columnNames;
    }
}
