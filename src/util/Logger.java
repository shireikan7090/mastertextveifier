package util;

import java.util.HashMap;
import java.util.Map;

public class Logger
{
    public static final int LOG_LEVEL_INFO = 0;
    public static final int LOG_LEVEL_WARNING = 1;
    public static final int LOG_LEVEL_ERROR = 2;
    public static final Map<Integer, String> LOG_LEVEL_TEXT = new HashMap<>();

    static
    {
        LOG_LEVEL_TEXT.put(LOG_LEVEL_INFO, "INFO");
        LOG_LEVEL_TEXT.put(LOG_LEVEL_WARNING, "WARN");
        LOG_LEVEL_TEXT.put(LOG_LEVEL_ERROR, "ERROR");
    }

    public static final String LOG_FORMAT = "[%s] %s, %s, %s";

    public static void infoLog(String clazz, String method, String... message)
    {
        log(LOG_LEVEL_INFO, clazz, method, message);
    }

    public static void warningLog(String clazz, String method, String... message)
    {
        log(LOG_LEVEL_WARNING, clazz, method, message);
    }

    public static void errorLog(String clazz, String method, String... message)
    {
        log(LOG_LEVEL_ERROR, clazz, method, message);
    }

    private static void log(int logLevel, String clazz, String method, String... message)
    {
        System.out.println(String.format(LOG_FORMAT, LOG_LEVEL_TEXT.get(logLevel), clazz, method,  String.join(",", message)));
    }
}
