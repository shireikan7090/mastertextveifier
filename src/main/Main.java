package main;

import module.AnalyzerManager;
import module.MasterManager;
import util.Define;
import util.Logger;

public class Main
{
    public static int _languageRegion;

    private static boolean init()
    {
//        MasterManager.getInstance().init();

        _languageRegion = Define.LANGUAGE_REGION_WW;

        return true;
    }

    public static int getLanguageRegion()
    {
        return _languageRegion;
    }

    public static void main(String[] args)
    {
        if (!init())
        {
            Logger.errorLog(Main.class.getName(), "main", "initialize failed");

            return;
        }
        Logger.infoLog(Main.class.getName(), "main", "initialize success");
        AnalyzerManager.getInstance().analyze();
    }
}
