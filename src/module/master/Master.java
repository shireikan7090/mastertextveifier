package module.master;

import org.json.JSONObject;
import util.Define;
import util.Logger;

import java.io.*;

public abstract class Master
{
    protected static JSONObject _rawJson = new JSONObject();

    public Master(String masterName)
    {
        if (!loadMaster(masterName))
        {
            return;
        }
    }

    public static JSONObject getRawJson()
    {
        return _rawJson;
    }

    private boolean loadMaster(String masterName)
    {
        JSONObject jsonObject = new JSONObject(loadJSONString(masterName));
        if (jsonObject == null || jsonObject.length() <= 0)
        {
            Logger.errorLog(getClass().getName(), "loadMaster", "json parse failed");
            return false;
        }
        _rawJson = jsonObject;

        return true;
    }

    private String loadJSONString(String path)
    {
        String masterText = "";
        try
        {
            final String PATH = Define.MASTER_BASE_DIRECTORY + path;
            Logger.infoLog(getClass().getName(), "loadToString", PATH);

            File file = new File(PATH);
            BufferedReader br = new BufferedReader(new FileReader(file));

            String tmp;
            while ((tmp = br.readLine()) != null)
            {
                masterText += tmp;
            }
            Logger.errorLog(getClass().getName(), "loadToString", masterText);

            br.close();

            return masterText;
        }
        catch (FileNotFoundException e)
        {
            Logger.errorLog(getClass().getName(), getClass().getEnclosingMethod().getName(), "file not found");
        }
        catch (IOException e)
        {
            Logger.errorLog(getClass().getName(), getClass().getEnclosingMethod().getName(), "file load failed");
        }

        return new JSONObject().toString();
    }
}
