package module.master;

import org.json.JSONObject;

public class ItemMaster
    extends Master
{
    public static final String MASTER_NAME = "ItemMaster";
    public static final String MASTER_FILE_NAME = "item.spt";

    public ItemMaster()
    {
        super(MASTER_FILE_NAME);
    }

    public static JSONObject getRawJson()
    {
        return new JSONObject()
                .put("1", new JSONObject()
                    .put("description", "SSR武器確定チケットガチャ")
                    .put("description_de", "SSR武器確定チケットガチャ")
                    .put("description_es", "SSR武器確定チケットガチャ")
                    .put("description_fr", "SR武器確定チケットガチャ")
                    .put("description_it", "SSR武器確定チケットガチャ")
                    .put("description_pt", "SSR防具確定チケットガチャ")
                    .put("description_zh", "SSR武器確定チケットガチャ")
                    .put("description_kr", "SSR武器確定チケットガチャ")
                );
    }
}
