package module;

import com.google.common.reflect.ClassPath;
import module.analyzer.Analyzer;
import module.analyzer.RarityAnalyzer;
import org.atilika.kuromoji.Tokenizer;
import util.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class AnalyzerManager {
    public static final String USER_DICTIONARY_PATH = "lib/dic/user_dic.csv";

    private static AnalyzerManager _this = null;
    private static Tokenizer _tokenizer = null;
    private static Map<String, Analyzer> _analyzers = new HashMap<>();

    protected AnalyzerManager()
    {
        if (_tokenizer == null)
        {
            _tokenizer = Tokenizer.builder().build();
        }
        // ユーザ定義辞書をねじ込む
        _tokenizer = loadUserDictionary();

        if (!loadAnalyzers())
        {
            Logger.errorLog(AnalyzerManager.class.getName(), "constructor", "load analyzer failed");
            return;
        }
        Logger.infoLog(AnalyzerManager.class.getName(), "constructor", "load analyzer succeed");
    }

    public static AnalyzerManager getInstance()
    {
        if (_this == null)
        {
            _this = new AnalyzerManager();
        }
        return _this;
    }

    public void analyze()
    {
        for(Map.Entry<String, Analyzer> entry : _analyzers.entrySet())
        {
            entry.getValue().analyze();
        }
    }

    private static boolean loadAnalyzers()
    {
        try
        {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            Set<Class<?>> allClasses = ClassPath.from(loader)
                    .getTopLevelClasses("module.analyzer").stream()
                    .map(info -> info.load())
                    .collect(Collectors.toSet());

            Pattern p = Pattern.compile("([a-zA-Z0-9]+)$");
            Map<String, Analyzer> analyzers = new HashMap<>();
            for (Class<?> clazz : allClasses)
            {
                String className = "";
                Matcher m = p.matcher(clazz.getName());
                if (m.find())
                {
                    className = m.group(0);
                }

                Analyzer analyzer;
                switch (className)
                {
                    case RarityAnalyzer.ANALYZER_NAME:
                        analyzer = new RarityAnalyzer(_tokenizer);
                        break;
                    default:
                        continue;
                }
                if (analyzer != null)
                {
                    analyzers.put(className, analyzer);
                }
            }
            _analyzers = analyzers;
        }
        catch (IOException e)
        {
            Logger.errorLog(AnalyzerManager.class.getName(),
                    AnalyzerManager.class.getEnclosingMethod().getName(),
                    "classloader initialize failed");

            return false;
        }
        return true;
    }

    private Tokenizer loadUserDictionary()
    {
        Tokenizer.Builder builder = new Tokenizer.Builder();
        try
        {
            Tokenizer tokenizer = builder.userDictionary(USER_DICTIONARY_PATH).build();
            if(tokenizer == null)
            {
                throw new Exception("tokenizer fix failed");
            }
            return tokenizer;
        }
        catch (IOException e)
        {
            Logger.errorLog(getClass().getName(), "loadUserDictionary", "user dictionary load failed");
        }
        catch (Exception e)
        {
            Logger.errorLog(getClass().getName(), "loadUserDictionary", e.getMessage());
        }
        return _tokenizer;
    }
}
