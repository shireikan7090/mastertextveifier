package module;

import com.google.common.reflect.ClassPath;
import module.master.ItemMaster;
import module.master.Master;
import util.Logger;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MasterManager
{
    private static MasterManager _this = null;
    private static Map<String, Master> _masters = new HashMap<>();

    public static MasterManager getInstance()
    {
        if (_this == null)
        {
            _this = new MasterManager();
        }
        return _this;
    }


    public void init()
    {
        if (!loadMasters())
        {
            Logger.errorLog(MasterManager.class.getName(), "constructor", "load master failed");
        }
        Logger.infoLog(MasterManager.class.getName(), "constructor", "load master succeed");
    }

    private static boolean loadMasters()
    {
        try
        {
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            Set<Class<?>> allClasses = ClassPath.from(loader)
                .getTopLevelClasses("module.master").stream()
                .map(info -> info.load())
                .collect(Collectors.toSet());

            Pattern p = Pattern.compile("([a-zA-Z0-9]+)$");
            Map<String, Master> masters = new HashMap<>();
            for (Class<?> clazz : allClasses)
            {
                String className = "";
                Matcher m = p.matcher(clazz.getName());
                if (m.find())
                {
                    className = m.group(0);
                }

                Master master;
                switch (className)
                {
                    case ItemMaster.MASTER_NAME:
                        master = new ItemMaster();
                        break;
                    default:
                        continue;
                }
                if (master != null)
                {
                    masters.put(className, master);
                }
            }
            _masters = masters;
        }
        catch (IOException e)
        {
            Logger.errorLog(MasterManager.class.getName(),
                            MasterManager.class.getEnclosingMethod().getName(),
                            "classloader initialize failed");

            return false;
        }

        return true;
    }
}
