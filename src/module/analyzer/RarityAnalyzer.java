package module.analyzer;

import main.Main;
import module.master.ItemMaster;
import org.atilika.kuromoji.Tokenizer;
import org.json.JSONObject;
import util.Define;
import util.Logger;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static util.Define.RARITIES;

public class RarityAnalyzer extends Analyzer {
    public static final String ANALYZER_NAME = "RarityAnalyzer";

    public RarityAnalyzer(Tokenizer tokenizer)
    {
        super(tokenizer);
    }

    @Override
    public void analyze()
    {
        JSONObject itemMaster = ItemMaster.getRawJson();
        for(String keyStr : itemMaster.keySet())
        {
            JSONObject json = itemMaster.getJSONObject(keyStr);
            JSONObject targetJson = new JSONObject();
            for(String columnName : Define.getLocalizedColumnName(Main.getLanguageRegion(), "description"))
            {
                targetJson.put(columnName, json.getString(columnName));
            }

            Set<String> rarities = new HashSet<>();
            for(String key : targetJson.keySet())
            {
                List<String> tokenStrs = tokenize((String)json.get(key)).stream().map(o -> o.getSurfaceForm()).collect(Collectors.toList());
                for(String r : RARITIES)
                {
                    if(tokenStrs.contains(r))
                    {
                        if(0 < rarities.size() && !rarities.contains(r))
                        {
                            Logger.errorLog(getClass().getName(), "analyze", "一つのアイテムで複数のレアリティが含まれています。id: " + keyStr + ", column:" + key);
                        }
                        rarities.add(r);
                    }
                }
            }
        }
    }

    @Override
    public String getAnalyzerName() {
        return ANALYZER_NAME;
    }
}
