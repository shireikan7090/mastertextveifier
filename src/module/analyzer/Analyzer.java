package module.analyzer;

import org.atilika.kuromoji.Token;
import org.atilika.kuromoji.Tokenizer;

import java.util.List;

public abstract class Analyzer
{
    protected static Tokenizer _tokenizer;

    public Analyzer(Tokenizer tokenizer)
    {
        _tokenizer = tokenizer;
    }

    public List<Token> tokenize(String str)
    {
        return _tokenizer.tokenize(str);
    }

    public abstract String getAnalyzerName();

    public abstract void analyze();
}
